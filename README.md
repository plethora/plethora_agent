<div style="text-align: center">
    <img src="logo.png" alt="plethora logo">
    <p>a nagios-like application aiming at the IOT world!</p>
</div>

<hr>

# | Agent

The agent is the part of the app that is in charge of collecting and sending data about a device using SNMP protocol.

## Installation

Just run the java jar after having deployed the modules you want to use

## Contributors

+ [Constant Deschietere](mailto:constant.deschietere@cpe.fr)
+ [Rudy Deal](mailto:rudy.deal@cpe.fr)
+ [Maxime Collot](mailto:maxime.collot@cpe.fr)
+ [Florian Croisot](mailto:constant.deschietere@cpe.fr)
+ [Jordan Quagliatini](mailto:jordan.quagliatini@cpe.fr)
