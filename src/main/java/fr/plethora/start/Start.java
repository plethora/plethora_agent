package fr.plethora.start;

import fr.plethora.task.Agent;
import org.apache.log4j.Logger;

import java.util.Timer;

/**
 * Entry point of the plethora agent
 */
public class Start {
	private final static Logger logger = Logger.getLogger(Start.class);
	private final static int EVERY_TEN_SECONDS = 10_000;
	
	public static void main(String[] args) throws Exception {
	    logger.info("Launching Agent");
        Agent agent = new Agent();
        agent.init();
        logger.info("Scheduling the agent");
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(agent, 0, EVERY_TEN_SECONDS);
        logger.info("End of initialization");
	}
}
