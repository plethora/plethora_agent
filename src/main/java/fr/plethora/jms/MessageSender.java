package fr.plethora.jms;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import fr.plethora.properties.PropKeys;
import org.apache.log4j.Logger;

import java.util.Properties;

public class MessageSender {
    private final static Logger logger = Logger.getLogger(MessageSender.class);

    private final static String QUEUE_NAME = "plethora";

    public static void send(String message, Properties prop) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(prop.getProperty(PropKeys.RMQ_ADDRESS.toString()));
            factory.setPort(Integer.valueOf(prop.getProperty(PropKeys.RMQ_PORT.toString())));
            factory.setUsername(prop.getProperty(PropKeys.RMQ_USERNAME.toString()));
            factory.setPassword(prop.getProperty(PropKeys.RMQ_PASSWORD.toString()));
            factory.setVirtualHost(prop.getProperty(PropKeys.RMQ_VHOST.toString()));
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.queueDeclare(
                prop.getProperty(PropKeys.RMQ_QUEUE.toString()),
                    true, false, false, null);
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
            logger.info(message);
            channel.close();
            connection.close();
        } catch (Exception e) {
            //TODO save the data in case of error
            logger.error("An error occured while sending message to the RabbitMQ", e);
        }
    }
}
