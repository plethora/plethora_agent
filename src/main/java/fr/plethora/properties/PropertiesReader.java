package fr.plethora.properties;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.util.Properties;

/**
 * Class used to read the property file containing some agent specific settings
 */
public class PropertiesReader {
    private final static Logger logger = Logger.getLogger(PropertiesReader.class);
    private static Properties prop = new Properties();
    private static InputStream input = null;

    /**
     * Parse the config file and returns the property found
     * @return the property found
     */
    public static Properties parse() {
        try {
            input = new FileInputStream(System.getenv("PLETHORA_CONF") + "config.properties");
            // load a properties file
            prop.load(input);
            prop.setProperty(PropKeys.MODULES.toString(), System.getenv("PLETHORA_CONF") + "modules/");
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (System.getenv("PLETHORA_CONF") == null) {
                logger.error("The environment variable 'PLETHORA_CONF' should be defined !");
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error("An error occurred while closing property file", e);
                }
            }
        }
        return prop;
    }
}
