package fr.plethora.properties;

/**
 * Enum containing the keys used in the config.properties file
 */
public enum PropKeys {
    FIRST_RUN,
    ADDRESS_SNMP,
    ID,
    MODULES,
    RMQ_ADDRESS,
    RMQ_PORT,
    RMQ_USERNAME,
    RMQ_PASSWORD,
    RMQ_VHOST,
    RMQ_QUEUE
}
