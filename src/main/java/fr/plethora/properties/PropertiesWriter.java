package fr.plethora.properties;

import org.apache.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertiesWriter {

    private final static Logger logger = Logger.getLogger(PropertiesWriter.class);
    private static OutputStream output = null;

    public static void run(Properties prop) {
        logger.info("Writing properties to PLETHORA_CONF file");
        try {
            output = new FileOutputStream(System.getenv("PLETHORA_CONF"));
            prop.store(output, null);
        } catch (Exception e) {
            logger.error("Can't save the updated configuration !",e);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    logger.error("An error occurred while closing property file", e);
                }
            }
        }
    }
}
