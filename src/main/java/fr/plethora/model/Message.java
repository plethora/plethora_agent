package fr.plethora.model;

import java.sql.Timestamp;

public class Message {
    private String device_id;
    private String module_id;
    private long timestamp;
    private Data data;

    public void prepare(String device_id, String module_id) {
        this.device_id = device_id;
        this.module_id = module_id;
        timestamp = new Timestamp(System.currentTimeMillis()).getTime();
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp.getTime();
    }

    public Data getData() {
        return data;
    }

    public void setData(String key, String value) {
        this.data = new Data(key, value);
    }

    public void setData(Data data) {
        this.data = data;
    }

}
