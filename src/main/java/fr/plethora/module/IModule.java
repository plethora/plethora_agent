package fr.plethora.module;

import fr.plethora.model.Data;
import fr.plethora.snmp.SNMPManager;

public interface IModule {
    int getInterval();
    Data getResult(SNMPManager manager);
    String getModuleName();
}
