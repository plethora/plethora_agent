package fr.plethora.task;

import com.google.gson.Gson;
import fr.plethora.jms.MessageSender;
import fr.plethora.model.Data;
import fr.plethora.model.Message;
import fr.plethora.module.IModule;
import fr.plethora.properties.PropKeys;
import fr.plethora.properties.PropertiesReader;
import fr.plethora.properties.PropertiesWriter;
import fr.plethora.snmp.SNMPManager;
import fr.plethora.utils.DefaultValues;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Timestamp;
import java.util.*;

public class Agent extends TimerTask {
    private final static Logger logger = Logger.getLogger(Agent.class);
    private final static String HEARTBEAT = "HEARTBEAT";
    private final static String UP = "UP";

    private static Properties prop = null;
    private SNMPManager client;
    private Gson gson = new Gson();
    private Map<Integer, List<IModule>> moduleMap = new HashMap<>();
    private Map<Integer, Timestamp> lastRunModules = new HashMap<>();

    public void init() throws Exception {
        logger.info("Initializing agent");
        initConfig();
        logger.info("Initialization tests");
        if (DefaultValues.TRUE.toString().equals(prop.getProperty(PropKeys.FIRST_RUN.toString()))) {
            logger.info("Setting some properties for the first time");
            String uuid = UUID.randomUUID().toString();
            prop.setProperty(PropKeys.ID.toString(), uuid);
            prop.setProperty(PropKeys.FIRST_RUN.toString(), DefaultValues.FALSE.toString());
            saveConfig(prop);
        }
        generateModuleMap();
        client = new SNMPManager(prop.getProperty(PropKeys.ADDRESS_SNMP.toString()));
        client.start();
    }

    @Override
    public void run() {
        logger.info("Launching the agent default task");
        Message message = new Message();
        message.setData(HEARTBEAT, UP);
        message.prepare(prop.getProperty(PropKeys.ID.toString()), "DEFAULT");
        MessageSender.send(gson.toJson(message), prop);
        runModules();
        logger.info("All the agent work is done");
    }

    /**
     * Init the configuration of the agent
     */
    private static void initConfig() throws Exception {
        logger.info("Reading configuration file located at PLETHORA_CONF");
        prop = PropertiesReader.parse();
        if (prop == null) {
            throw new Exception("An occurred while reading configuration file");
        }
    }

    private static void saveConfig(Properties props) {
        logger.info("Saving configuration file");
        PropertiesWriter.run(props);
    }

    private void generateModuleMap() {
        File[] files = new File(prop.getProperty(PropKeys.MODULES.toString())).listFiles();

        for (File file : files) {
            if (file.isFile() && file.getName().contains(".jar")) {
                IModule module = getModule(file.getAbsolutePath());
                if (module == null) {
                    continue;
                }
                int interval = module.getInterval();
                List<IModule> jars = moduleMap.get(interval);
                if (jars == null) {
                    jars = new ArrayList<>();
                    lastRunModules.put(interval, null);
                }
                jars.add(module);
                moduleMap.put(interval, jars);
            }
        }
    }

    private IModule getModule(String path) {
        try {
            File myJar = new File(path);
            URL[] urls = new URL[]{myJar.toURL()};
            URLClassLoader child = new URLClassLoader(urls, this.getClass().getClassLoader());
            Class classToLoad = Class.forName("fr.plethora.module.Module", true, child);
            Object instance = classToLoad.newInstance();
            return (IModule) instance;
        } catch (Exception e) {
            logger.error("Couldn't launch module named :" + path + "!", e);
        }
        return null;
    }

    private void runModules() {
        logger.info("Launching all the modules");
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        for (Integer key : lastRunModules.keySet()) {
            if (lastRunModules.get(key) == null || (currentTime.getTime() - lastRunModules.get(key).getTime()) > key) {
                for(IModule module : moduleMap.get(key)) {
                    Data data = module.getResult(client);
                    Message message = new Message();
                    message.setData(data);
                    message.prepare(prop.getProperty(PropKeys.ID.toString()), module.getModuleName());
                    MessageSender.send(gson.toJson(message), prop);
                }
                lastRunModules.put(key, currentTime);
            }
        }
    }
}
