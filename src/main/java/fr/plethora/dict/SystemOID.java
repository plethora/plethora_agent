package fr.plethora.dict;

import org.snmp4j.smi.OID;

public class SystemOID {
    public OID HARDWARE_DESC = new OID(".1.3.6.1.2.1.1.1.0");
    public OID SYS_UPTIME = new OID(".1.3.6.1.2.1.1.3.0");
    public OID NAME = new OID(".1.3.6.1.2.1.1.5.0");

    public OID MEM_QTY;
    public OID CPU_NAME;
}
